require('dotenv').config()
const webPush = require('web-push');
const http = require('http')
const express = require('express')
const cors = require('cors')
const path = require('path')
const bodyParser = require('body-parser')
const app = require('express')()

const PUBLIC_DIR = process.env.PUBLIC_DIR || '../public'

app.use(cors())
app.use(bodyParser.json());

if (!process.env.VAPID_PUBLIC_KEY || !process.env.VAPID_PRIVATE_KEY) {
  console.log("You must set the VAPID_PUBLIC_KEY and VAPID_PRIVATE_KEY "+
    "environment variables. You can use the following ones:")
  console.log(webPush.generateVAPIDKeys())
  return;
}

webPush.setVapidDetails(
  //'https://serviceworke.rs/',
  'http://localhost',
  process.env.VAPID_PUBLIC_KEY,
  process.env.VAPID_PRIVATE_KEY
);

let g_subscriptions = []

function routes(app, route) {
  app.get(route + 'vapidPublicKey', function(req, res) {
    console.log('request for vapid public key')
    res.send(process.env.VAPID_PUBLIC_KEY)
  })

  app.post(route + 'register', function(req, res) {
    const subscription = req.body.subscription

    // avoid subscriping the same client multiple times
    // don't know why the client sends this every time it's started though
    if (g_subscriptions.filter(x => x.keys.p256dh === subscription.keys.p256dh).length === 0) {
      g_subscriptions.push(subscription)
      console.log('register')
    }
    res.sendStatus(201)
  })
  app.post(route + 'message', (req, res) => {
    console.log('post: message')
    // TODO error checking
    const msg = req.body.msg
    const payload = msg
    const options = {
      TTL: req.body.ttl
    }

    g_subscriptions.map(s => (
      webPush.sendNotification(s, payload, options)
        .then(function() {
          res.sendStatus(201)
        }).catch((err) => {
          console.error('webPush: ', err)
        })
    ))
  })
}

// TODO this should be middle-ware
routes(app, '/')

// TODO this needs some error checking
app.use(express.static(path.resolve(__dirname, PUBLIC_DIR)))
app.get("/", (req, res) => {
    res.send('Something wrong with the html files.')
})

const PORT = process.env.PORT || 3003
app.listen(PORT, () => {
    console.log(`Server running on port ${PORT}`)
})
