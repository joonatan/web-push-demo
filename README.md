# Push notification demo
Made for testing and demonstrating push notifications with pure javascript/html.

Deployed in [Heroku](https://murmuring-fjord-82353.herokuapp.com/)

Can be installed on mobile phones.

Pushes work on both desktop Chrome and Android Phones. iOS not tested.

All messages cause a broadcast push (on all devices).

Uses NodeJS for the backend. Only Javascript, html and css for the front-end.

The service worker is the minimal one you can have for a PWA and WebPush.

## Deployment

Clone the repo

Define the following environment variables.
```
# WebPush keys
VAPID_PUBLIC_KEY
VAPID_PRIVATE_KEY
# public folder where the index.html lives
PUBLIC_DIR
```

then
``` bash
yarn
yarn start
```

or
``` bash
docker build -t push-test ./
docker run -p 2000:80 --rm push-test
```
