FROM node:12.13.1-alpine as build
WORKDIR /app

ENV PATH /app/node_modules/.bin:$PATH
COPY package.json /app/package.json
RUN npm install --silent

# Copy build files
COPY public/ /app/public
COPY src/* /app/
COPY .env /app/

EXPOSE 80

ENV PORT 80
ENV PUBLIC_DIR /app/public
CMD ["node", "/app/index.js"]
