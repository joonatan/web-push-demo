const CACHE_NAME = 'push-notification-cache-v1'
const urlsToCache = []

self.addEventListener('install', (event) => {
  console.log('The service worker is being installed.')
})

// TODO add a cache scheme
// either use network first or cache first with network update
// using cache first makes development and testing impossible
self.addEventListener('fetch', (event) => {
  event.respondWith(fetch(event.request))
})

self.addEventListener('activate', (event) => {
})

// TODO this requires some work now
// Google Dev tools use the text format, but we want to send a message to the server
//  and receive an echo in json data format
// Even more confusing, if we use the server to send a payload we can't read it from data...
//  data = null then
self.addEventListener('push', event => {
  const txt = event.data ? event.data.text() : 'no-payload'

  console.log('notification: ', txt);
  event.waitUntil(
    self.registration.showNotification('Push', {
      body: txt
    })
  )
});

self.addEventListener('notificationclose', function(e) {
});

self.addEventListener('notificationclick', event => {
  const notification = event.notification
  notification.close()
  // TODO how to get data?
  console.log(notification.data)
  // Don't waitUntil here, it doesn't load the local window
  self.clients.openWindow("/")
})
