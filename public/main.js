toBase64 = function (u8) {
  return btoa(String.fromCharCode.apply(null, u8));
}

fromBase64 = function (str) {
  return atob(str).split('').map(function (c) { return c.charCodeAt(0); });
}

if('serviceWorker' in navigator) {
  navigator.serviceWorker.register('service-worker.js')
    .then((registration) => {
    console.log('Service Worker is registered');
    return registration.pushManager.getSubscription()
        .then(async (subscription) => {
          if (subscription) {
            return subscription;
          }

          console.log('No valid subscription: creating a new one.');
          const response = await fetch( '/vapidPublicKey', {
              method: 'GET',
              headers: new Headers({ "Content-Type": "text/plain" }),
            }
          )
          const vapidPublicKey = await response.text()
          if (vapidPublicKey === '') {
            throw Error('no public key received')
          }

          const convertedVapidKey = (vapidPublicKey)

          return registration.pushManager.subscribe({
            userVisibleOnly: true,
            applicationServerKey: convertedVapidKey
          })
        })
    }).then((subscription) => {
      console.log('Registering for updates')
      fetch('/register', {
        method: 'post',
        headers: {
          'Content-type': 'application/json'
        },
        body: JSON.stringify({
          subscription: subscription
        })
      })
    }).catch((error) => {
      console.error('Service Worker Error', error);
    });
} else {
  console.log('Your browser does not support the Service-Worker!')
}

const msg_input = document.getElementById("message");

async function onSend () {
  await fetch('/message', {
    method: 'post',
    headers: { 'Content-type': 'application/json' },
    body: JSON.stringify({
      msg: msg_input.value,
    })
  })

  // cleanup
  msg_input.value = ''
}
